<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enum;
use App\Models\Task;
use App\Enum\TaskStatus;
use App\Http\Requests\CreateTask;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $task = Task::all();
            return response()->json($task);
        } catch (\Throwable $th) {
            return response()->json([
                "error" => $th->getMessage(),
                "message" => "No se pudieron listar las tareas"
            ]);
        }
       
    }

    public function listTrashed()
    {
        try {
            $task = Task::all();
            return response()->json($task->trashed());
        } catch (\Throwable $th) {
            return response()->json([
                "error" => $th->getMessage(),
                "message" => "No se pudieron listar las tareas"
            ]);
        }
       
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateTask $request)
    {
        try {
            $request->validated();
     
            $task = Task::create([
                'title' => $request->title,
                'description' => $request->description,
                'status' => TaskStatus::InProgress,
                'due_date' => now()
            ]);
    
            return response()->json([
                'message' => 'La tarea ha sido creada'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                "error" => $th->getMessage(),
                "message" => "No se pudo guardar la tarea",
            ]);
        }
       
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        if($id == null || $id){
            return response()->json([
                'message' => 'Ha ocurrido un error de validación, hace falta el ID',
            ], 400);
        }

        $task = Task::find($id);
        return response()->json($task);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CreateTask $request, string $id)
    {
        if($id == null || $id){
            return response()->json([
                'message' => 'Ha ocurrido un error de validación, hace falta el ID',
            ], 400);
        }

        $task = Task::find($id);
        $task->update($request);

        return response()->json($task);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if($id == null || $id){
            return response()->json([
                'message' => 'Ha ocurrido un error de validación, hace falta el ID',
            ], 400);
        }

       
        $task = Task::find($id);
        
        $task->delete();

        return response()->json([
            "message" => "Tarea borrada correctamente"
        ]);

    }
    
}
