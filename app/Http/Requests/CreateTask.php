<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
          'title' => 'required',
          'description' => 'required',
          'status' => 'required'  
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => 'El :attribute es obligatorio.',
            'description.required' => 'El :attribute es obligatorio.',
            'status.required' => 'El :attribute es obligatorio.'
        ];
    }
}
