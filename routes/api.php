<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

use App\Http\Controllers\TaskController;
use App\Http\Controllers\Auth\AuthController;


Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
});

Route::group(["middleware" => ["jwt.routes"]], function () {
    Route::get('task', 'index');
    Route::post('task', 'store');
    Route::get('task/{id}', 'show');
    Route::put('task/{id}', 'update');
    Route::delete('task/{id}', 'destroy');
});
